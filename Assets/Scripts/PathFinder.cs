﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PathFinder : MonoBehaviour {

    public WayPoints[] routes;
    public int pathIndex = 0;
    //Used to map the lanes after the scout wave
    public List<int> laneIndex = new List<int>();
	// Use this for initialization
	void Start () {
        pathIndex = -1;
        for (int i = 0; i < routes.Length; i++)
        {
            laneIndex.Add(i);
        }
	}

    public WayPoints getRoute()
    {
        
        Debug.Log(pathIndex);
        return routes[pathIndex];
    }

    public WayPoints getWayPoint()
    {
        pathIndex = Random.Range(laneIndex[0], laneIndex.Count);
        Debug.Log(pathIndex);
        return routes[pathIndex];
    }

    

    
}
