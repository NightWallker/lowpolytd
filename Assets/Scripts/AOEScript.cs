﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AOEScript : MonoBehaviour
{

    public float damage;
    public float slowPercent = 0.5f;
    public ParticleSystem effect;
    void DamageAndSlow(Transform enemy)
    {
         
        Enemy e = enemy.GetComponent<Enemy>();
        if (e != null)
        {
            
            e.damagePer5 = damage;
            e.isSlowed = !e.isSlowed;
            effect.Play(!effect.isPlaying);
        }

    }
    void OnTriggerEnter(Collider collider)
    {

        if (collider.tag == "Enemy")
        {

            DamageAndSlow(collider.transform);
            

        }

    }

    void OnTriggerExit(Collider collider)
    {

        if (collider.tag == "Enemy")
        {

            DamageAndSlow(collider.transform);
             

        }

    }
}
