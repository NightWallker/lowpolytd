﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class PlayerStats : MonoBehaviour {
    
    public static int Money;
    public int startMoney = 400;
    public static int Lives;
    public int startLives = 10;
    public Text healthCount;

    public static int waves;
     void Start()
    {
        healthCount.text = startLives.ToString();
        Money = startMoney;
        Lives = startLives;
        waves = 0;
    }

    void Update()
    {

        healthCount.text = Lives.ToString();
    }


}
