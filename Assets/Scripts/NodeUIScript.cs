﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class NodeUIScript : MonoBehaviour
{

    private Node target;
    public GameObject buildUI;
    public GameObject upgradeUi;
    public Text upgradeCost;
    public Text sellAmount;
    public Button upgradeButton;
    public void SetTarget(Node _target)
    {
        target = _target;

         transform.position = target.GetUIPosition();
        if (!target.isUpgraded && target.hasTurret)
        {
            buildUI.SetActive(false);
            upgradeCost.text = "Upgrade: $" + target.turretBluePrint.upgradeCost;
            upgradeButton.interactable = true;
        }
        if (!target.hasTurret)
        {
            upgradeUi.SetActive(false);
        }
        if(target.hasTurret && target.isUpgraded)
        {
            buildUI.SetActive(false);
            upgradeButton.interactable = false;
            upgradeCost.text = "Maximum";

        }
        if (target.hasTurret)
        {
            
                target = _target;
                upgradeUi.SetActive(true);
                upgradeUi.transform.position = target.GetUIPosition();
                sellAmount.text = "Sell: $" + target.turretBluePrint.GetSellAmount();
            
        }
        
        else                    
           buildUI.SetActive(true);
                     
          
    }

    public void Hide()
    {
        buildUI.SetActive(false);
        upgradeUi.SetActive(false);
    }

    public void Upgrade()
    {
        target.UpgradeTurret();
        BuildManager.instance.DeselectNode();

    }

    public void Sell()
    {
        target.SellTurret();
        target.isUpgraded = false;
        BuildManager.instance.DeselectNode();
    }
}
