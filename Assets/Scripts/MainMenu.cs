﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenu : MonoBehaviour {

    public string levelToLoad;

    public SceneFader sceneFader;

    public void Play()
    {
        sceneFader.FadeToBlack(levelToLoad);
    }

    public void Quit()
    {
         
        Application.Quit();
    }
	 
}
