﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour {

    public static bool gameOver;

    public GameObject gameOverUI;
    public GameObject completeLevelUI;
    public CompleteLevel nextLevel;
    public PauseMenu pauseMenu;
    public SceneFader sceneFader;


    void Start()
    {
        gameOver = false;
    }

    // Update is called once per frame
    void Update () {
        if (gameOver)
            return;

        

		if(PlayerStats.Lives <= 0)
        {
            EndGame();
        }
	}

    public void Retry()
    {
        pauseMenu.gameObject.SetActive(false);
        Time.timeScale = 1;
        completeLevelUI.SetActive(false);
        sceneFader.FadeToBlack(SceneManager.GetActiveScene().name);
        Debug.Log(SceneManager.GetActiveScene().name);
        
    }


    private void EndGame()
    {
        gameOver = true;
        gameOverUI.SetActive(true);
        Debug.Log("GAME OVER");
        //POP-UP MENU
    }

    public void WinLevel()
    {
        PlayerPrefs.SetInt("levelReached", nextLevel.levelToUnlock);
        completeLevelUI.SetActive(true);
    }
}
