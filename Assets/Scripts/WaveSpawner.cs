﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WaveSpawner : MonoBehaviour {

    public static int EnemiesAlive = 0;
    public static bool gameStarted;
    public Wave[] waves;
    public Transform spawnPoint;

    public float timeBetweenWaves = 5.5f;
    public float timeBetweenEnemies = 0.5f;

    public Text waveCountDownText;
    public Text wavePerLevel;
    public GameManager gameManager;
    public Button startButton;

    private float countdown = 1f;
    public int waveIndex = 0;

     void Start()
    {
        EnemiesAlive = 0;
        gameStarted = false;
        startButton.gameObject.SetActive(true);
    }
        

    void Update()
    {
       
          
           
            //Starting CD after enemies are dead for current wave
            if (EnemiesAlive > 0)
            {
                return;
            }

            if (waveIndex == waves.Length)
            {

                gameManager.WinLevel();
                this.enabled = false;

            }

            if (countdown <= 0f && gameStarted)
            {
                StartCoroutine(SpawnWave());
                countdown = timeBetweenWaves;
                return;
            }
        if (gameStarted)
        {
            wavePerLevel.text = ("Waves" + waveIndex + "/" + waves.Length);
            countdown -= Time.deltaTime;
            waveCountDownText.text = Mathf.Round(countdown).ToString();
        }
        
    }

    IEnumerator SpawnWave()
    {
     
        PlayerStats.waves++;
        
        Wave wave = waves[waveIndex];
        
        EnemiesAlive = wave.count;

        waveIndex++;
        for (int i = 0; i < wave.count; i++)
        {
            SpawnEnemy(wave.enemy);
            yield return new WaitForSeconds(1 / wave.rate);
        }

        


    }

    public void StartGame()
    {
        gameStarted = true;
        startButton.gameObject.SetActive(false);
    }


    void SpawnEnemy(GameObject enemy)
    {

        //TODO: Spawn different Types of Enemies
        Instantiate(enemy, spawnPoint.position, spawnPoint.rotation);
        
        
    }
	 
}
