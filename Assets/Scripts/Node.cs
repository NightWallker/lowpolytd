﻿
using UnityEngine;
using UnityEngine.EventSystems;

public class Node : MonoBehaviour
{

    public Color hoverColor;
    public Color notEnoughMoneyColor;
    public bool hasTurret;
    private Color startColor;

    public Vector3 positionOffset;
    public Vector3 UIOffset;
    public Vector3 rotationOffset;
    private Renderer rend;
    [HideInInspector]
    public GameObject turret;
    [HideInInspector]
    public TurretBlueprint turretBluePrint;
    public bool isUpgraded = false;

    BuildManager buildManager;


    void Start()
    {
        rend = GetComponent<Renderer>();
        startColor = rend.material.color;
        buildManager = BuildManager.instance;
        hasTurret = false;
    }

    public Vector3 GetBuildPosition()
    {
        return transform.position + positionOffset;
    }
    public Vector3 GetUIPosition()
    {
        return transform.position + UIOffset;
    }
    void OnMouseDown()
    {
        //Are we over GUI
        if (EventSystem.current.IsPointerOverGameObject())
            return;
  
        buildManager.SelectNode(this);
           return;
        
    }

    public void BuildTurret(TurretBlueprint blueprint)
    {
        if (PlayerStats.Money < blueprint.cost)
        {
            Debug.Log("Not enough gold");
            return;
        }
        PlayerStats.Money -= blueprint.cost;
        GameObject Turret = (GameObject)Instantiate(blueprint.prefab, GetBuildPosition(), Quaternion.Euler(rotationOffset));
        turret = Turret;

        turretBluePrint = blueprint;
        hasTurret = true;
        //Create effect for every turret
        GameObject effect = (GameObject)Instantiate(buildManager.buildEffect, GetBuildPosition(), Quaternion.identity);
        Destroy(effect, 4f);


        Debug.Log("Turret Upgraded! Money left:" + PlayerStats.Money);
    }

    public void UpgradeTurret()
    {
        if (PlayerStats.Money < turretBluePrint.upgradeCost)
        {
            Debug.Log("Not enough gold");
            return;
        }
        PlayerStats.Money -= turretBluePrint.upgradeCost;

        //Get rid of old turret
        Destroy(turret);

        //Building new one
        GameObject Turret = (GameObject)Instantiate(turretBluePrint.upgradedPrefab, GetBuildPosition(), Quaternion.Euler(rotationOffset));
        turret = Turret;




        //Create effect for every turret
        GameObject effect = (GameObject)Instantiate(buildManager.buildEffect, GetBuildPosition(), Quaternion.identity);
        Destroy(effect, 4f);

        isUpgraded = true;

        Debug.Log("UPGRADED TOWER");

    }

    public void SellTurret()
    {
        PlayerStats.Money += turretBluePrint.GetSellAmount();     
        turretBluePrint = null;
        isUpgraded = false;
        hasTurret = false;
        Destroy(turret);
        // Spawn cool effect
        GameObject effect = (GameObject)Instantiate(buildManager.sellEffect, GetBuildPosition(), Quaternion.identity);
        Destroy(effect, 4f);
        
    }

    void OnMouseEnter()
    {
        //if (!buildManager.CanBuild)
        //    return;
        if(!hasTurret)
        rend.material.color = hoverColor;
         
        else
        {
           rend.material.color = notEnoughMoneyColor;
        }
    }


    private void OnMouseExit()
    {
        rend.material.color = startColor;
    }
}
