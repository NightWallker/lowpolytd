﻿
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameOver : MonoBehaviour {

    
    public SceneFader sceneFader;
    public string menuScene = "MainMenu";
    

    public void Menu()
    {
        sceneFader.FadeToBlack(menuScene);
    }

}
