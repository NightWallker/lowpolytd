﻿using System;
using UnityEngine;

public class BulletScript : MonoBehaviour {

    private Transform target;

    public GameObject effect;
    public bool hasEffects;
    public int damage = 50;
    public float speed = 70f;
    public float explosionRadius = 0f;

    public void LockTarget(Transform _target)
    {
        target = _target;
    }
	
	// Update is called once per frame
	void Update () {
		
        if(target == null)
        {
            Destroy(this.gameObject);
            return;
        }

        Vector3 dir = target.position - transform.position;
        float distanceThisFrame = speed * Time.deltaTime;

        if(dir.magnitude <= distanceThisFrame)
        {
            HitTarget();
            return;
        }

        transform.Translate(dir.normalized * distanceThisFrame, Space.World);
        transform.LookAt(target);


	}

    private void HitTarget()
    {
        GameObject effectIns = (GameObject)Instantiate(effect, transform.position, transform.rotation);
        Destroy(effectIns, 5f);
        if (explosionRadius > 0f)
        {
           
            Explode();
           

        }
        else
        {
            Damage(target);
        }
        
        Destroy(this.gameObject);
        
    }
    
    void Explode()
    {
        Collider[] hitboxes = Physics.OverlapSphere(transform.position, explosionRadius);
        foreach (var collider in hitboxes)
        {
            if(collider.tag == "Enemy")
            {               
                Damage(collider.transform);
  
            }
        }
    }

    

   

    void Damage(Transform enemy)
    {
        Enemy e = enemy.GetComponent<Enemy>();
        if(e != null)
        {
            e.TakeDamage(damage);
        }
  
    }

    void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, explosionRadius);
    }
}
