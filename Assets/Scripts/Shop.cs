﻿using UnityEngine;

public class Shop : MonoBehaviour
{
    public TurretBlueprint standartTurret;
    public TurretBlueprint frozenTurret;
    public TurretBlueprint cannonTurret;
    public TurretBlueprint chaosTurret;
    BuildManager buildManager;


    private void Start()
    {
        buildManager = BuildManager.instance;
    }

    public void SelectCannonTurret()
    {
        Debug.Log("Turret Bought");
      
        buildManager.selectedNode.BuildTurret(cannonTurret);
        buildManager.DeselectNode();
    }

    public void SelectFrostTurret()
    {
        Debug.Log("Turret Bought");
        
        buildManager.selectedNode.BuildTurret(frozenTurret);
        buildManager.DeselectNode();
    }

    public void SelectStandartTurret()
    {
        Debug.Log("Standart Turret Bought");
        buildManager.selectedNode.BuildTurret(standartTurret);
        buildManager.DeselectNode();
    }

    public void SelectChaosTurret()
    {
        Debug.Log("Chaos Turret Bought");
        buildManager.selectedNode.BuildTurret(chaosTurret);
        buildManager.DeselectNode();
    }



}
