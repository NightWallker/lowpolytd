﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Enemy : MonoBehaviour {

    public EnemyMovement movement;
    [SerializeField]
    private int EnemyID;
    
    public float startSpeed = 10f;
    [HideInInspector]
    public float speed;
    public float startHealth = 100;
    private float health;
    public int goldDrop = 50;
    private bool isDead = false;
    
    public GameObject deathEffect;
    [Header("SLOW EFFECT")]
    public bool isSlowed;
    public float slowCountDown;
    public float slowRate;
    public float damagePer5;

    [Header("Unity Stuff")]
    public Image healthBar;

    void Start()
    {
        movement = GetComponent<EnemyMovement>();
        speed = startSpeed;
        health = startHealth;
    }

    public void Update()
    {
        if (isSlowed)
        {
            speed /= 3;
           
            if (slowCountDown <= 0f)
            {
                TakeDamage(damagePer5);
                slowCountDown = 1f / slowRate;

            }

            slowCountDown -= Time.deltaTime;
        }
        else
            speed = startSpeed;
    }

    public void TakeDamage(float amount)
    {
         
        health -= amount;
        healthBar.fillAmount = health / startHealth;
        if (health <= 0 && !isDead)
        {
            Die();
        }
    }

    void Die()
    {
        isDead = true;
        PlayerStats.Money += goldDrop;

        GameObject dieEffect = (GameObject)Instantiate(deathEffect, transform.position, Quaternion.identity);        
        Destroy(dieEffect, 3);

        WaveSpawner.EnemiesAlive--;
        //Removes the lane where the scout died
        movement.path.laneIndex.Remove(movement.scoutID);
        Destroy(gameObject);
    }

    public void Slow(float percent)
    {
        speed = startSpeed * (1f - percent);
    }


    

}
