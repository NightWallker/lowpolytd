﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildManager : MonoBehaviour {

    public static BuildManager instance;

    private TurretBlueprint turretToBuild;
    public Node selectedNode;
    public NodeUIScript nodeUI;
     
    public GameObject buildEffect;
    public GameObject sellEffect;
    [SerializeField]
    public bool CanBuild { get { return turretToBuild != null; } }
    public bool HasMoney { get { return PlayerStats.Money >= turretToBuild.cost; } }

    private void Awake()
    {
        if(instance != null)
        {
            Debug.LogError("More than one Buildmanager at Scene!");
        }

        instance = this;
        
    }

 
    public void SelectNode(Node node)
    {
        if(selectedNode == node)
        {
            DeselectNode();
            return;
        }
        selectedNode = node;
        turretToBuild = null;

        nodeUI.SetTarget(node);
        

        

        
    }     

    public void DeselectNode()
    {
        selectedNode = null;
        nodeUI.Hide();
        
    }
    public void SelectTurretToBuild(TurretBlueprint turret)
    {
        turretToBuild = turret;
       // selectedNode = null;
        nodeUI.Hide();

    }

    public TurretBlueprint GetTurretToBuild()
    {
        return turretToBuild;
    }

    
}
