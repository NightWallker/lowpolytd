﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CompleteLevel : MonoBehaviour {

    public SceneFader levelFader;

    private string mainMenu = "MainMenu";
    public string levelToLoad = "Level2";
    public int levelToUnlock = 2;

    public void Continue()
    {
        levelFader.FadeToBlack(levelToLoad);
        Debug.Log("LEVEL WON");
        
    }

    public void Menu()
    {
        levelFader.FadeToBlack(mainMenu);
    }
}
