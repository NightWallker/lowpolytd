﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tower : MonoBehaviour
{

    private Enemy targetEnemy;

    [Header("Attributes")]
    public float range = 15f;


    [Header("Use Bullets(Default)")]
    public float fireRate = 1f;
    private float fireCountDown = 0f;
    public GameObject bulletPrefab;


    [Header("Use Laser")]
    public bool useLaser = false;
    public int damageOverTime = 20;
    public float slowPercent = 0.5f;
    public LineRenderer lineRend;
    public ParticleSystem impactEffect;

    [Header("Use AOE")]
    public bool useAOE = false;
    public GameObject AOEPrefab;
  //  public ParticleSystem AOEEffect;
    [Header("Settings")]
    [SerializeField]
    private Transform target;
    public Transform partToRotate;

    public Transform firePoint;





    // Use this for initialization
    void Start()
    {
        InvokeRepeating("UpdateTarget", 0f, 0.5f);

    }

    //Finds Target, invokes every 0.5fs
    void UpdateTarget()
    {
        GameObject[] enemies = GameObject.FindGameObjectsWithTag("Enemy");
        float shortestDistance = Mathf.Infinity;
        GameObject nearestEnemy = null;


        foreach (var enemy in enemies)
        {

            float distanceToEnemy = Vector3.Distance(transform.position, enemy.transform.position);
            if (distanceToEnemy < shortestDistance)
            {
                shortestDistance = distanceToEnemy;
                nearestEnemy = enemy;
            }
        }

        if (nearestEnemy != null && shortestDistance <= range)
        {
            target = nearestEnemy.transform;
            targetEnemy = nearestEnemy.GetComponent<Enemy>();
        }
        else
        {
            target = null;
        }


    }


    // Update is called once per frame
    void Update()
    {
        if (target == null)
        {
            if (useLaser)
            {
                if (lineRend.enabled)
                {
                    lineRend.enabled = false;
                    impactEffect.Stop();
                }
                    
            }
            if (useAOE)
            {
                AOEPrefab.SetActive(false);
                
            }
            return;
        }

        LockOnTarget();

        if (useLaser)
        {
            Laser();
        }
        if (useAOE)
        {
            AOE();
        }
        else
        {
            if (fireCountDown <= 0f)
            {
                Shoot();
                fireCountDown = 1f / fireRate;

            }

            fireCountDown -= Time.deltaTime;
        }
    }

    void LockOnTarget()
    {

        //Target lock-on
        Vector3 dir = target.position - transform.position;
        Quaternion lookRatation = Quaternion.LookRotation(dir);
        Vector3 rotation = Quaternion.Lerp(partToRotate.rotation, lookRatation, Time.deltaTime * 10).eulerAngles;
        partToRotate.rotation = Quaternion.Euler(0f, rotation.y, 0);
    }

    void Laser()
    {
        targetEnemy.TakeDamage(damageOverTime * Time.deltaTime);

        targetEnemy.Slow(slowPercent);

        if (!lineRend.enabled)
        {
            lineRend.enabled = true;
            impactEffect.Play();
        }


        lineRend.SetPosition(0, firePoint.position);
        lineRend.SetPosition(1, target.position);

        Vector3 dir = firePoint.position - target.position;

        impactEffect.transform.position = target.position + dir.normalized;

        impactEffect.transform.rotation = Quaternion.LookRotation(dir);

       
    }

    void AOE()
    {
        AOEPrefab.SetActive(true);
       
      
    }

    void Shoot()
    {
        GameObject bulletGO = (GameObject)Instantiate(bulletPrefab, firePoint.position, firePoint.rotation);
        BulletScript bullet = bulletGO.GetComponent<BulletScript>();

        if (bullet != null)
        {
            bullet.LockTarget(target);
        }
    }

    void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, range);
    }
}
