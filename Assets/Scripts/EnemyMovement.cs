﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Enemy))]
public class EnemyMovement : MonoBehaviour
{


    private Enemy enemy;
    [SerializeField]
    private Transform target;
    private int wavePointIndex = 0;
    private WayPoints wayPoint;
    public PathFinder path;
    [Header("Use for scout units only")]
    public bool isScout = false;
    public int scoutID;

    // Use this for initialization
    void Start()
    {
        path = FindObjectOfType<PathFinder>();
        enemy = GetComponent<Enemy>();
        //Gets Random Route
        if (!isScout)
        {
            wayPoint = path.getWayPoint();
            target = wayPoint.points[0];
        }
        //Gets Specific Route
        if (isScout)
        {
            path.pathIndex++;
            scoutID = path.pathIndex;
            Debug.Log(path.pathIndex);
            wayPoint = path.getRoute();
            target = wayPoint.points[0];
            
        }
       

    }

    void Update()
    {
        Vector3 dir = target.position - transform.position;
        transform.Translate(dir.normalized * enemy.speed * Time.deltaTime, Space.World);


        if (Vector3.Distance(transform.position, target.position) <= 1f)
        {
            GetNextWayPoint();
        }

        enemy.speed = enemy.startSpeed;
    }

    void GetNextWayPoint()
    {
        if (wavePointIndex >= wayPoint.points.Length - 1)
        {
            EndPath();
            return;
        }
        wavePointIndex++;
        target = wayPoint.points[wavePointIndex];
    }

    void EndPath()
    {
        PlayerStats.Lives--;
        //Removes the laneIndex where the scout passed successfuly
        path.laneIndex.Remove(scoutID);  
        WaveSpawner.EnemiesAlive--;
        Destroy(gameObject);

    }

}
